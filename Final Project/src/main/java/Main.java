// Does this need a package to work?

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.*;

@WebServlet(name = "Main", urlPatterns = {"/Main"})
public class Main extends HttpServlet {
    double answer = 0;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter writer = response.getWriter();
        response.setContentType("text/html");
        writer.println("<html><head></head><body>");
        try {
            String type1 = request.getParameter("type1");
            double input1 = Double.parseDouble(request.getParameter("input1"));
            String type2 = request.getParameter("type2");
            double output = convert(input1, type1, type2);
            writer.println("<h1>Conversion Results</h1>");
            writer.println("<h4>" + input1 + " " + type1 + " when converted to " + type2 + " is: <br><br>" + output + " " + type2 + "</h4>");
            writer.println("</body></html>");
        } catch (NumberFormatException e) {
            writer.println("<h1> ---Error--- </h1>");
            writer.println("You must enter a number. Please go back and try again. <br><br>");
            writer.println(e.toString());
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        writer.println("Error message.");
    }

    public double convert(double input1, String type1, String type2) {
        if (type1.equals("USD")) {
            switch (type2) {
                case "USD":
                    answer = input1 * 1;
                    break;
                case "BS":
                    answer = input1 * 6.9;
                    break;
                case "Yen":
                    answer = input1 * 103.53;
                    break;
                default:
                    answer = 1010101; // Random number to indicate to me that something is wrong
            }
        } else if (type1.equals("BS")) {
            switch (type2) {
                case "BS":
                    answer = input1 * 1;
                    break;
                case "USD":
                    answer = input1 * .15;
                    break;
                case "Yen":
                    answer = input1 * 15.01;
                default:
                    answer = 1010101; // Random number to indicate to me that something is wrong
            }
        } else if (type1.equals("Yen")) {
            switch (type2) {
                case "Yen":
                    answer = input1 * 1;
                    break;
                case "USD":
                    answer = input1 * .0097;
                    break;
                case "BS":
                    answer = input1 * .067;
                    break;
                default:
                    answer = 1010101; // Random number to indicate to me that something is wrong
            }
        } else {
            answer = 50505050; // Random number to indicate to me that something is wrong
        }
        if (answer < 0) {
            System.out.printf("User input is a positive number = %B%n", false);
        } else {
            System.out.printf("User input is a positive number = %B%n", true);
        }

        return answer;
    }
}
