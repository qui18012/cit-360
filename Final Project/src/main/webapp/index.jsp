<%--
  Created by IntelliJ IDEA.
  User: aquin
  Date: 12/3/2020
  Time: 1:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Currency Converter</title>
</head>
<body>
<h1>Convert Your Currency!</h1>
<form action="Main" method="post">               <%--action might be "main"--%>
    <label>Choose your starting currency: </label>         <!-- add a for tag here -->
    <select name="type1" id="type1">
        <option value="USD">USD</option>
        <option value="BS">BS</option>
        <option value="Yen">Yen</option>
    </select>
    <br>
    <p> Enter amount here: <input type="text" name="input1" size="13"> </p>
    <br>
    <label>Choose your ending currency: </label>         <!-- add a for tag here -->
    <select name="type2" id="type2">
        <option value="USD">USD</option>
        <option value="BS">BS</option>
        <option value="Yen">Yen</option>
    </select>
    <br><br>
    <p><input type="submit" value="Convert!"/></p> <!-- I had the input type set to "button" instead of "submit"
                                                        which cost me more time than I'd like to admit to fix... -->

</form>
</body>
</html>
