import Hibernate.Employee;
import Hibernate.EmployeeDAO;
import Hibernate.RunHibernate;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet(name = "Servlet1" , urlPatterns = {"/Servlet1"})
public class Servlet1 extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("hello world");

        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        Integer phone = Integer.parseInt(request.getParameter("phone"));
        Integer id = Integer.parseInt(request.getParameter("id"));




            //statement.executeUpdate("insert into employee_info values ("+id+", "+firstname+", "+lastname+","+phone+")");


            out.println(id);
            out.println(firstname);
            out.println(lastname);
            out.println(phone);


        RunHibernate hi = new RunHibernate();
        hi.run();


        }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly, you must go to /employee.html and submit the form  ");


    }
}
