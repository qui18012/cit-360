package Hibernate;
//to test connection

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;


public class EmployeeDAO {

    SessionFactory factory = null;
    Session session = null;

    private static EmployeeDAO single_instance = null;

    private EmployeeDAO()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    public static EmployeeDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new EmployeeDAO();
        }

        return single_instance;
    }

    /** Used to get more than one Employee from database.*/
    public List<Employee> getEmployees() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Hibernate.Employee";
            List<Employee> cs = (List<Employee>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /** Used to get a single Employee from database */
    public Employee getEmployee(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Hibernate.Employee where id=" + Integer.toString(id);
            Employee b = (Employee) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return b;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    



}