package Hibernate;

import javax.persistence.*;


@Entity
@Table(name= "employee_info")
public class Employee {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name= "id")
    private int id;

    @Column (name= "firstname")
    private String firstname;

    @Column (name= "lastname")
    private String lastname;

    @Column (name= "phone")
    private int phone_number;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstname;
    }

    public void setFirstName(String firstname) {
        this.firstname = firstname;
    }

    public String getLastName() {
        return lastname;
    }

    public void setLastName(String lastname) {
        this.firstname = lastname;
    }

    public int getPhoneNumber(){ return phone_number;}

    public void setPhoneNumber(int phone_number){this.phone_number = phone_number;}

    public String toString() {
        return Integer.toString(id) + " " + firstname + " " + lastname + " " + phone_number;
    }





}
