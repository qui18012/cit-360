import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class Convert {

    public static String carToJson(Car car) {
        ObjectMapper map = new ObjectMapper();
        String s = "";

        try {
            s = map.writeValueAsString(car);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return s;
    }

    public static Car JSONToCar(String s) {
        ObjectMapper map = new ObjectMapper();
        Car car = null;

        try {
            car = map.readValue(s, Car.class); // mapper needs to know what to convert S into
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return car;
    }

}