public class Car {
    private String make;
    private String model;
    private long mileage;

    public void setMake(String make) {
        this.make = make;
    }

    public void setMileage(long mileage) {
        this.mileage = mileage;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public long getMileage() {
        return mileage;
    }

    @Override
    public String toString() {
        return "Your car is a " + make +
                " " + model +
                ", with " + mileage +
                " miles on it. ";
    }
}