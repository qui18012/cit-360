import java.net.*;
import java.io.*;
import java.util.*;

public class Client {
    public static String getHttpContent(String string) {

        String input = "";

        try {
            URL address = new URL(string);
            HttpURLConnection http = (HttpURLConnection) address.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder builder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                builder.append(line + "\n"); //  This is in the case of there being multiple lines.
            }
            input = builder.toString();
        } catch (Exception e) {
            System.err.println(e.toString());
        }
        return input;
    }
}
