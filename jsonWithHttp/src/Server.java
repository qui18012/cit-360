import com.sun.net.httpserver.*;
import java.io.*;
import java.net.InetSocketAddress;

public class Server {

    public static HttpServer getServer() throws IOException{
        HttpServer server = HttpServer.create(new InetSocketAddress(8001), 0);
        server.createContext("/test", new MyHandler());
        server.setExecutor(null);
        return server;
    }


    static class MyHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {

            Car firstCar = new Car();           // This is probably not a good practice, but I decided
            firstCar.setMake("Dodge");          // to create my object here in the handler.
            firstCar.setModel("Dart");
            firstCar.setMileage(75000);         // Fun fact, this is my actual car and its actual mileage.

            String json = Convert.carToJson(firstCar); // converting recently created object into a json, then to a string.
            t.sendResponseHeaders(200, json.getBytes().length);
            OutputStream os = t.getResponseBody();
            os.write(json.getBytes());
            os.close();
        }
    }
}
