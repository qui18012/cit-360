import com.sun.net.httpserver.HttpServer;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Starting Server...");
        HttpServer server = Server.getServer(); // makes call to server.java to create a server
        server.start(); // starts the server that was created

        String input = Client.getHttpContent("http://localhost:8001/test"); // reception of server data
        Car outputCar = Convert.JSONToCar(input);               // Here I create a new car object
        System.out.println("Data Received: \n" + outputCar);    // and here I display the object to the user
                                                                // by automatically using the .toString method.
        System.out.println("Server shutting down...");
        server.stop(0);
        System.out.println("Finished");
    }
}
