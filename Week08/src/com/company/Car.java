package com.company;

import java.util.*;

public class Car implements Runnable{

    private String make;
    private String model;
    Random r = new Random();
    int time;

    public Car(String make, String model) {
        this.make = make;
        this.model = model;
        time = r.nextInt(1500); // This is the syntax to assign a random variable between 1 and 1500.
    }

    public void run() {
        try {
            System.out.printf("\n%s %s is driving for %d milliseconds\n", make, model, time);
            Thread.sleep(time);
            System.out.printf("\n%s %s is finished driving.\n", make, model);
        } catch (Exception e) {
            System.out.println("-------ERROR!-------\n");
            System.err.println(e.toString());
        }
    }
}
