package com.company;

public class Main {

    public static void main(String[] args) {
        // Simply creating the threads using the constructor in the Car class,
        // and starting them.
        Thread threadOne = new Thread(new Car("Toyota", "Camry"));
        Thread threadTwo = new Thread(new Car("Ford", "F150"));
        Thread threadThree = new Thread(new Car("Honda", "Civic"));
        Thread threadFour = new Thread(new Car("Tesla", "Model X"));
        Thread threadFive = new Thread(new Car("Jeep", "Wrangler"));
        Thread threadSix = new Thread(new Car("Hyundai", "Elantra"));

        threadOne.start();
        threadTwo.start();
        threadThree.start();
        threadFour.start();
        threadFive.start();
        threadSix.start();

    }
}
