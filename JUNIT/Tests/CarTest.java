import org.junit.Test;

import static org.junit.Assert.*;

public class CarTest {

    @Test
    public void getName() {
        Car dart = new Car("Dart");
        assertEquals("Dart", dart.getName());
    }

    @Test
    public void getMileage() {
        Car dart = new Car("Dart");
        assertEquals(6000, dart.getMileage());
    }
}