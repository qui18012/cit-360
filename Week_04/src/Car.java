public class Car {
    private final String make;
    private final String model;
    private final long mileage;

    public Car(String make, String model, long mileage) {
        this.make = make;
        this.model = model;
        this.mileage = mileage;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public long getMileage() {
        return mileage;
    }

    @Override
    public String toString() {
        return "Your car is a " +
                "Make = " + make + '\'' +
                " Model = " + model + '\'' +
                ", with " + mileage + '\'' +
                "miles on it. ";
    }
}
