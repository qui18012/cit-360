import org.junit.Test;

import static org.junit.Assert.*;

public class junitTest {

    @Test
    public void arrayEqualsExample() {
        int[] expected = {1,2,3};
        assertArrayEquals(expected, junit.arrayEqualsExample());
    }

    @Test
    public void equalsExample() {
        assertEquals(5, junit.equalsExample());
    }

    @Test
    public void falseExample() {
        assertFalse(junit.falseExample());
    }

    @Test
    public void notNullExample() {
        assertNotNull(junit.notNullExample());
    }

    @Test
    public void notSameExample() {
        String word = "Bye";
        assertNotSame(word, junit.notSameExample());
    }

    @Test
    public void nullExample() {
        assertNull(junit.nullExample());
    }

    @Test
    public void sameExample() {
        String word = "Hi";
        assertSame(word, junit.sameExample());
    }

    @Test
    public void trueExample() {
        assertTrue(junit.trueExample());
    }
}