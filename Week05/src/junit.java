public class junit {
    /**
     *  To run all the tests, I right clicked
     * "jUnitTest" test class and clicked "run jUnitTest"
     */
    public static void main(String[] args) {
        System.out.println("\nInitiating tests...");
        arrayEqualsExample();
        equalsExample();
        falseExample();
        notNullExample();
        notSameExample();
        nullExample();
        sameExample();
        trueExample();
        System.out.println("\nTesting complete...");
    }
    public static int[] arrayEqualsExample() {
        int[] input = {1, 2, 3};
        try {
            System.out.println("\nThis is a test of assertArrayEquals.");
        } catch (ArithmeticException e) {
            System.err.println("Somehow you got an error.");
        }
        return input;
    }

    public static int equalsExample() {
        try {
            System.out.println("This is a test of assertEquals.");
        } catch (ArithmeticException e) {
            System.err.println("Somehow you got an error.");
        }
        return 5;
    }

    public static boolean falseExample() {
        try {
            System.out.println("This is a test of assertFalse.");
        } catch (ArithmeticException e) {
            System.err.println("Somehow you got an error.");
        }
        return false;
    }

    public static String notNullExample() {
        try {
            System.out.println("This is a test of assertNotNull.");
        } catch (ArithmeticException e) {
            System.err.println("Somehow you got an error.");
        }
        return "Hi";
    }

    public static String notSameExample() {
        try {
            System.out.println("This is a test of assertNotSame.");
        } catch (ArithmeticException e) {
            System.err.println("Somehow you got an error.");
        }
        return "Hi";
    }

    public static String nullExample() {
        try {
            System.out.println("This is a test of assertNull.");
        } catch (ArithmeticException e) {
            System.err.println("Somehow you got an error.");
        }
        return null;
    }

    public static String sameExample() {
        try {
            System.out.println("This is a test of assertSame");
        } catch (ArithmeticException e) {
            System.err.println("Somehow you got an error.");
        }
        return "Hi";
    }

    public static boolean trueExample() {
        try {
            System.out.println("This is a test of assertTrue");
        } catch (ArithmeticException e) {
            System.err.println("Somehow you got an error.");
        }
        return true;
    }

}
