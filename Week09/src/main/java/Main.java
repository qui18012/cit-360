import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class Main {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");

        /** First Client */
        Client client1 = new Client();
        Client client2 = new Client();
        client1.setId(2);
        client1.setName("Billy");
        client1.setAge(28);
        client1.setPhone_number(1234);

        /** Second Client */
        client2.setId(3);
        client2.setName("Gilly");
        client2.setAge(41);
        client2.setPhone_number(2468);


        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.persist(client1);
        entityManager.persist(client2);

        entityManager.getTransaction().commit();

        entityManagerFactory.close();
    }
}
