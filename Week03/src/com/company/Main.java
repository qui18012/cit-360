package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);

        System.out.println("\nHello. Please enter two numbers, this" +
                " program will then divide them for you\n");
        int x = 1; // counter used to end our do-while loop
        do {
            try {

                System.out.println("Enter the first number: ");
                int n1 = userInput.nextInt();
                System.out.println("Enter the second number: ");
                int n2 = userInput.nextInt();
                float output = divide(n1, n2);
                System.out.println("The answer is " + output + "!");
                x = 2;
            } catch (ArithmeticException e) { // specific exception
                System.out.println("You can't divide by zero, try again.\n");
            } catch (InputMismatchException e) {
                System.out.println("That's not a number, please type in a number.\n");
                userInput.nextLine(); // This clears the scanner to prevent infinite loop.
            } finally {
                System.out.println("(This is the finally statement).\n");
            } // end of try catch finally blocks
        } while (x == 1); // end of do-while loop
    } // end of main

    public static int divide(int n1, int n2) throws ArithmeticException {
        return n1 / n2;
    }
} // end of class
