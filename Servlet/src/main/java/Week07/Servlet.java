package Week07;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter writer = response.getWriter();
        response.setContentType("text/html");
        writer.println("<html><head></head><body>");
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        writer.println("<h1>Information</h1>");
        writer.println("<h4>Name: " + name + "</h4>");
        writer.println("<h4>Email: " + email + "</h4>");
        writer.println("</body></html>");
        System.out.println("\n-----NOTICE-----\n");
        System.out.printf("\n\nUser has entered \"%s\" as their name\n", name);
        System.out.printf("User has entered \"%s\" as their email address\n", email);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        writer.println("Error message.");
    }
}
