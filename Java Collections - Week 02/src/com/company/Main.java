package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        System.out.println("---LIST---"); // Very simple collection
        List list = new ArrayList();
        list.add("Toyota");
        list.add("Ford");
        list.add("Dodge");
        list.add("Chrysler");
        list.add("Audi");
        list.add("Toyota");

        for (Object str : list) {
            System.out.println((String) str);
        }
        System.out.println("---------");

        System.out.println("---SET---");
        Set set = new TreeSet();
        set.add("Toyota"); // This won't be added because sets only accept one copy of each kind
        set.add("Ford");
        set.add("Dodge");
        set.add("Chrysler");
        set.add("Audi");
        set.add("Toyota");

        for (Object str : set) {
            System.out.println((String) str);
        }
        System.out.println("----------");

        System.out.println("---QUEUE---");
        Queue queue = new PriorityQueue(); // Priority will order all the items for you.
        queue.add("Toyota");
        queue.add("Ford");
        queue.add("Dodge");
        queue.add("Chrysler");
        queue.add("Audi");
        queue.add("Toyota");


        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }
        System.out.println("---------");


        System.out.println("---MAP---");
        Map map = new HashMap();
        map.put(1, "Toyota");
        map.put(2, "Ford");
        map.put(3, "Dodge");
        map.put(4, "Chrysler");
        map.put(3, "Audi"); // This will be read into the map, and will take Dodge's place because they're both #3.
        map.put(5, "Toyota");

        for (int i = 1; i < 6; i++) {
            String result = (String) map.get(i);
            System.out.println(result);
        }
    }
}
